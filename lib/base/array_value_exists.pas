function array_value_exists(arr : dastr; str : string) : boolean;
var
  i : longword;
  r : boolean;
begin
  i := 0;
  r := false;
  if length(arr) > 0
    then repeat
      if arr[i] = str
      then r := true;
      i := i + 1;
    until (i >= length(arr));
  array_value_exists := r;
end;
