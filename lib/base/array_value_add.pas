procedure array_value_add(var arr : daint; val : integer);
begin
  setlength (arr, length(arr) + 1);
  arr[length(arr)-1] := val;
end;
procedure array_value_add(var arr : dastr; val : string);
begin
  setlength (arr, length(arr) + 1);
  arr[length(arr)-1] := val;
end;
