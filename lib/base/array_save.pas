procedure array_save (
	path : ansistring;
	arr : daint
);
var
  f : file of integer;
  i : longword;
begin
  assign(f, path);
  rewrite(f);
  i := 0;
  repeat
    write(f, arr[i]);
    i := i + 1;
  until i >= length(arr);
  close(f);
end;
procedure array_save (
	path : ansistring;
	arr : dastr
);
var
  f : file of string;
  i : longword;
begin
  assign(f, path);
  rewrite(f);
  i := 0;
  repeat
    write(f, arr[i]);
    i := i + 1;
  until i >= length(arr);
  close(f);
end;
