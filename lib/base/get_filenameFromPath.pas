function get_filenameFromPath (path : ansistring) : ansistring;
var
  n : longword;
begin
  for n := length (path) downto 1 do
  begin
    if path[n] = '\' then break;
    if path[n] = '/' then break;
  end;
  get_filenameFromPath := copy(path, n+1, length (path));
end;
