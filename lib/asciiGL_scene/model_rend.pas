//need fix
procedure model_rend (var frame : daint; data : daint; dataOffset : dalw; id : word;  x, y, d, m : integer);
var
  i, size, ofs : longword;
  c : byte;
  rx, ry, xc, yc, xt, yt, xc2, yc2, xt2, yt2, s, id2 : integer;
begin
  ofs := dataOffset[id];
  i := ofs;
  size := data [i+1];
  rX := data [i+4];
  rY := data [i+5];
  id2 := data[i];
  i := i + 6;
  if id = id2
  then repeat
      case data[i] of
      1: begin
          xt := data[i+1];
          yt := data[i+2];
          c := data[i+3];
          s := data[i+4];
          i := i + 5;
          case d of
          0 : begin
              xc := xt;
              yc := yt;
            end;
          1 : begin
              xc := yt;
              yc := -xt;
            end;
          2 : begin
              xc := -xt;
              yc := -yt;
            end;
          3 : begin
              xc := -yt;
              yc := xt;
            end;
          else begin
              xc := xt;
              yc := yt+3;
            end;
          end;

          case m of
          0 : begin
              xc := xc;
              yc := yc;
            end;
          1 : begin
              xc := -xc;
              yc := yc;
            end;
          2 : begin
              xc := xc;
              yc := -yc;
            end;
          3 : begin
              xc := -xc;
              yc := -yc;
            end;
          else begin
              xc := xc+2;
              yc := yc+2;
            end;
          end;
          xc := x+rx + xc;
          yc := y+ry + yc;
          if (xc >= 0) and (xc < frame[2]) and (yc >= 0) and (yc < frame[3])
          then setChXY(frame, xc, yc, c, s);
        end;
      2: begin
          //writeln('0');
          xt := data[i+1];
          yt := data[i+2];
          xt2 := data[i+3];
          yt2 := data[i+4];
          c := data[i+5];
          s := data[i+6];
          i := i + 7;
          case d of
          0 : begin
              xc := xt;
              yc := yt;
            end;
          1 : begin
              xc := yt;
              yc := -xt;
            end;
          2 : begin
              xc := -xt;
              yc := -yt;
            end;
          3 : begin
              xc := -yt;
              yc := xt;
            end;
          else begin
              xc := xt;
              yc := yt+3;
            end;
          end;

          case m of
          0 : begin
              xc := xc;
              yc := yc;
            end;
          1 : begin
              xc := -xc;
              yc := yc;
            end;
          2 : begin
              xc := xc;
              yc := -yc;
            end;
          3 : begin
              xc := -xc;
              yc := -yc;
            end;
          else begin
              xc := xc+2;
              yc := yc+2;
            end;
          end;
          xc := x+rx + xc;
          yc := y+ry + yc;

          case d of
          0 : begin
              xc2 := xt2;
              yc2 := yt2;
            end;
          1 : begin
              xc2 := yt2;
              yc2 := -xt2;
            end;
          2 : begin
              xc2 := -xt2;
              yc2 := -yt2;
            end;
          3 : begin
              xc2 := -yt2;
              yc2 := xt2;
            end;
          else begin
              xc2 := xt2;
              yc2 := yt2+3;
            end;
          end;
          case m of
          0 : begin
              xc2 := xc2;
              yc2 := yc2;
            end;
          1 : begin
              xc2 := -xc2;
              yc2 := yc2;
            end;
          2 : begin
              xc2 := xc2;
              yc2 := -yc2;
            end;
          3 : begin
              xc2 := -xc2;
              yc2 := -yc2;
            end;
          else begin
              xc2 := xc2+2;
              yc2 := yc2+2;
            end;
          end;
          xc2 := x+rx + xc2;
          yc2 := y+ry + yc2;
          setLine(frame, xc, yc, xc2, yc2, c, s);
        end
      else break;
      end; //case
    until i >= size + ofs
  else begin
    gotoxy(2, id);
    writeln(id,'!=',id2,'-',ofs,':id offset error');
  end;
end;
