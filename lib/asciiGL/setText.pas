procedure setText(var frame : daint; x,y,w,color: word;str: ansistring);
var
  l,i,n,xt : word;
begin
  l := length(str);
  i := 0;
  n := 0;
  repeat
    i := i + 1;
    if n > w
    then begin
      y := y + 1;
      n := 0;
    end;
    xt := x + n;
    setChXY(frame,xt,y,color,ord(str[i]));
    
    n := n + 1;
  until i = l;
end;
